#ifndef STREAM9_CONTAINER_REF_ARRAY_HPP
#define STREAM9_CONTAINER_REF_ARRAY_HPP

#include <concepts>
#include <functional>
#include <initializer_list>
#include <ranges>
#include <system_error>
#include <vector>

#include <stream9/container.hpp>
#include <stream9/errors.hpp>
#include <stream9/iterators.hpp>
#include <stream9/ranges/concepts.hpp>

namespace stream9::container {

namespace iter { using namespace stream9::iterators; }

class ref_array_base
{
public:
    enum class errc {
        index_is_out_of_range,
        capacity_is_too_large,
    };

    static std::error_category const& error_category();
};

/*
 * @model std::regular
 * @model std::ranges::random_access_range
 */
template<typename T>
    requires (!std::is_reference_v<T>)
class ref_array : public ref_array_base
{
    using base_t = std::vector<T*>;

public:
    class iterator;
    class const_iterator;

    using size_type = base_t::size_type;

public:
    // essentials
    ref_array() = default;

    template<typename R>
        requires rng::forward_range<R>
              && std::same_as<rng::value_t<R>, T>
    ref_array(R&&);

    ref_array(std::initializer_list<std::reference_wrapper<T>> init);

    ref_array(ref_array const&) = default;
    ref_array& operator=(ref_array const&) = default;

    ref_array(ref_array&&) = default;
    ref_array& operator=(ref_array&&) = default;

    // accessor
    T& at(size_type pos) const;
    T& operator[](size_type pos) const noexcept { return *m_array[pos]; }

    T& front() const noexcept { return *m_array.front(); }
    T& back() const noexcept { return *m_array.back(); }

    const_iterator begin() const noexcept { return m_array.begin(); }
    const_iterator end() const noexcept { return m_array.end(); }
    const_iterator cbegin() const noexcept { return m_array.cbegin(); }
    const_iterator cend() const noexcept { return m_array.cend(); }
    const_iterator rbegin() const noexcept { return m_array.rbegin(); }
    const_iterator rend() const noexcept { return m_array.rend(); }
    const_iterator crbegin() const noexcept { return m_array.crbegin(); }
    const_iterator crend() const noexcept { return m_array.crend(); }

    // query
    std::size_t size() const noexcept { return m_array.size(); }

    bool empty() const noexcept { return m_array.empty(); }

    std::size_t max_size() const noexcept { return m_array.max_size(); }

    std::size_t capacity() const noexcept { return m_array.capacity(); }

    // modifier
    void push_back(T& value); // no rvalue ref variant

    // no emplace_back

    iterator insert(const_iterator pos, T& value);

    template<std::input_iterator It>
        requires std::convertible_to<std::iter_reference_t<It>, T&>
    iterator insert(const_iterator pos, It first, It last);

    // no emplace

    iterator erase(const_iterator pos) noexcept;
    iterator erase(const_iterator first, const_iterator last) noexcept;

    void pop_back() noexcept;

    void clear() noexcept;

    void reserve(size_type new_cap);

    void shrink_to_fit();

    // no resize

    void swap(ref_array& other) noexcept;

    // ordering
    bool operator==(ref_array const&) const = default;
    std::strong_ordering operator<=>(ref_array const&) const = default;

private:
    std::vector<T*> m_array;
};

template<typename R>
    requires rng::input_range<R>
ref_array(R&&) -> ref_array<rng::value_t<R>>;

/*
 * definition
 */
template<typename T>
    requires (!std::is_reference_v<T>)
template<typename R>
    requires rng::forward_range<R>
          && std::same_as<rng::value_t<R>, T>
ref_array<T>::
ref_array(R&& r)
{
    if constexpr (rng::sized_range<R>) {
        m_array.reserve(rng::size(r));
    }

    for (auto&& e: r) {
        m_array.push_back(&e);
    }
}

template<typename T>
    requires (!std::is_reference_v<T>)
ref_array<T>::
ref_array(std::initializer_list<std::reference_wrapper<T>> init)
{
    m_array.reserve(init.size());

    for (auto const& e: init) {
        m_array.push_back(&e.get());
    }
}

template<typename T>
    requires (!std::is_reference_v<T>)
T& ref_array<T>::
at(size_type const pos) const
{
    try {
        return *m_array.at(pos);
    }
    catch (std::out_of_range const&) {
        throw_error(errc::index_is_out_of_range, {
            { "pos", pos },
            { "size", size() },
        });
    }
}

template<typename T>
    requires (!std::is_reference_v<T>)
void ref_array<T>::
push_back(T& value)
{
    try {
        m_array.push_back(&value);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T>
    requires (!std::is_reference_v<T>)
ref_array<T>::iterator ref_array<T>::
insert(const_iterator const pos, T& value)
{
    try {
        return m_array.insert(pos.base(), &value);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T>
    requires (!std::is_reference_v<T>)
template<std::input_iterator It>
    requires std::convertible_to<std::iter_reference_t<It>, T&>
ref_array<T>::iterator ref_array<T>::
insert(const_iterator const pos, It first, It const last)
{
    try {
        auto i = pos.base();

        while (first != last) {
            i = m_array.insert(i, &*first);
            ++i;
            ++first;
        }

        return m_array.erase(i, i);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T>
    requires (!std::is_reference_v<T>)
ref_array<T>::iterator ref_array<T>::
erase(const_iterator const pos) noexcept
{
    return m_array.erase(pos.base());
}

template<typename T>
    requires (!std::is_reference_v<T>)
ref_array<T>::iterator ref_array<T>::
erase(const_iterator const first,
      const_iterator const last) noexcept
{
    return m_array.erase(first.base(), last.base());
}

template<typename T>
    requires (!std::is_reference_v<T>)
void ref_array<T>::
pop_back() noexcept
{
    m_array.pop_back();
}

template<typename T>
    requires (!std::is_reference_v<T>)
void ref_array<T>::
clear() noexcept
{
    m_array.clear();
}

template<typename T>
    requires (!std::is_reference_v<T>)
void ref_array<T>::
reserve(size_type const new_cap)
{
    try {
        m_array.reserve(new_cap);
    }
    catch (std::length_error const&) {
        throw_error(errc::capacity_is_too_large, {
            { "new_cap", new_cap },
            { "max_size", max_size() },
        });
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T>
    requires (!std::is_reference_v<T>)
void ref_array<T>::
shrink_to_fit()
{
    try {
        m_array.shink_to_fit();
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T>
    requires (!std::is_reference_v<T>)
void ref_array<T>::
swap(ref_array& other) noexcept
{
    m_array.swap(other);
}

inline std::error_category const& ref_array_base::
error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept override
        {
            return "stream9::container::ref_array";
        }

        std::string message(int const ec) const override
        {
            switch (static_cast<errc>(ec)) {
                case errc::index_is_out_of_range:
                    return "index is out of range";
                case errc::capacity_is_too_large:
                    return "capacity is too large";
            }
            std::abort();
        }
    } instance;

    return instance;
}

inline std::error_code
make_error_code(ref_array_base::errc const e)
{
    return { static_cast<int>(e), ref_array_base::error_category() };
}

/*
 * class ref_array::iterator
 */
template<typename T>
    requires (!std::is_reference_v<T>)
class ref_array<T>::iterator
    : public iter::iterator_facade<ref_array::iterator,
                                   std::random_access_iterator_tag,
                                   T& >
{
    using base_t = std::vector<T*>::iterator;

public:
    iterator() = default;

    iterator(base_t const it)
        : m_it { it }
    {}

    base_t base() const noexcept
    {
        return m_it;
    }

private:
    friend class iter::iterator_core_access;

    T&
    dereference() const noexcept
    {
        return **m_it;
    }

    void
    increment() noexcept
    {
        ++m_it;
    }

    void
    decrement() noexcept
    {
        --m_it;
    }

    void
    advance(std::ptrdiff_t const n) noexcept
    {
        m_it += n;
    }

    bool
    equal(iterator const other) const noexcept
    {
        return m_it == other.m_it;
    }

    std::strong_ordering
    compare(iterator const other) const noexcept
    {
        return m_it <=> other.m_it;
    }

    std::ptrdiff_t
    distance_to(iterator const other) const noexcept
    {
        return other.m_it - m_it;
    }

private:
    base_t m_it {};
};

template<typename T>
    requires (!std::is_reference_v<T>)
class ref_array<T>::const_iterator
    : public iter::iterator_facade<ref_array::const_iterator,
                                   std::random_access_iterator_tag,
                                   T& >
{
    using base_t = std::vector<T*>::const_iterator;

public:
    const_iterator() = default;

    const_iterator(base_t const it)
        : m_it { it }
    {}

    const_iterator(iterator const it)
        : m_it { it.m_it }
    {}

    base_t base() const noexcept
    {
        return m_it;
    }

private:
    friend class iter::iterator_core_access;

    T&
    dereference() const noexcept
    {
        return **m_it;
    }

    void
    increment() noexcept
    {
        ++m_it;
    }

    void
    decrement() noexcept
    {
        --m_it;
    }

    void
    advance(std::ptrdiff_t const n) noexcept
    {
        m_it += n;
    }

    bool
    equal(const_iterator const other) const noexcept
    {
        return m_it == other.m_it;
    }

    std::strong_ordering
    compare(const_iterator const other) const noexcept
    {
        return m_it <=> other.m_it;
    }

    std::ptrdiff_t
    distance_to(const_iterator const other) const noexcept
    {
        return other.m_it - m_it;
    }

private:
    base_t m_it {};
};

} // namespace stream9::container

namespace std {

template<>
struct is_error_code_enum<stream9::container::ref_array_base::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_CONTAINER_REF_ARRAY_HPP
